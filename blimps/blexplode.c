/*    blexplode.c  Explode a blocks db into individual blocks 
           blexplode <input blocks file> [<prefix>]
	   Creates files named AC.blk
--------------------------------------------------------------------
 8/24/95 J. Henikoff
11/18/99 Added 2nd argument to prefix directory of output block files
====================================================================*/

#define BLNORM_C_
#define EXTERN
#define MAXNAME 80	/* Maximum file name length */

#include <blocksprogs.h>

/*=======================================================================*/

int main(argc, argv)
     int argc;
     char *argv[];
{
  FILE *bfp, *ofp;
  Block *block;
  char bdbname[MAXNAME], conname[MAXNAME], prefix[MAXNAME];
  int len;

/* ------------1st arg = blocks database -------------------------------*/
   if (argc > 1)
      strcpy(bdbname, argv[1]);
   else
   {
      printf("\nEnter name of blocks database: ");
      gets(bdbname);
   }
   if ( (bfp=fopen(bdbname, "r")) == NULL)
   {
      printf("\nCannot open file %s\n", bdbname);
      exit(-1);
   }

/* ------------2nd optional argument = prefix name----------------------*/
   prefix[0] = '\0';
   if (argc > 2) strcpy(prefix, argv[2]);

/*-----------------------------------------------------------------*/

  while ((block = read_a_block(bfp)) != NULL)
  {
     if (strlen(prefix))
     {
        sprintf(conname,"%s/%s", prefix, block->number);
/*
        strcpy(conname, prefix);
        if (strlen(block->number) == 8)
        {
           len = strlen(conname);
           conname[len] = block->number[7];
           conname[len+1] = '\0';
        }
*/
     }
     else
     { strcpy(conname, block->number); }
     strcat(conname, ".blk");
     if ( (ofp=fopen(conname, "w")) == NULL)
     {
         printf("\nCannot open file %s\n", conname);
         exit(-1);
     }
     output_block(block, ofp);
     fclose(ofp);
  }
   
  fclose(bfp);
  exit(0);

}  /* end of main */

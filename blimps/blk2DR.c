/* COPYRIGHT 2000 by the Fred Hutchinson Cancer Research Center
    blk2DR.c  	Makes list of sequences in blocks for expasy (DR format)
           blk2DR <input blocks file>
	   Creates file named <blocks file>.DR
--------------------------------------------------------------------
 3/28/00 J. Henikoff
====================================================================*/

#define BLNORM_C_
#define EXTERN
#define MAXNAME 80	/* Maximum file name length */

#include <blocksprogs.h>

void output_DR();

/*=======================================================================*/

int main(argc, argv)
     int argc;
     char *argv[];
{
  FILE *bfp, *ofp;
  Block *block;
  char bdbname[MAXNAME], prevfam[MAXNAME];
  char outname[MAXNAME];

/* ------------1st arg = blocks database -------------------------------*/
   if (argc > 1)
      strcpy(bdbname, argv[1]);
   else
   {
      printf("\nEnter name of blocks database: ");
      gets(bdbname);
   }
   if ( (bfp=fopen(bdbname, "r")) == NULL)
   {
      printf("\nCannot open file %s\n", bdbname);
      exit(-1);
   }

/*-----------------------------------------------------------------*/
   strcpy(outname, bdbname);
   strcat(outname, ".DR");
   if ( (ofp=fopen(outname, "w")) == NULL)
   {
         printf("\nCannot open file %s\n", outname);
         exit(-1);
   }

  prevfam[0] = '\0';
  while ((block = read_a_block(bfp)) != NULL)
  {
     if (strcmp(prevfam, block->family) != 0)  /* just process 1st block */
     {
        output_DR(block, ofp);
        strcpy(prevfam, block->family);
     }
  }
   
  fclose(bfp);
  fclose(ofp);

  exit(0);

}  /* end of main */
/*===================================================================
===================================================================*/
void output_DR(block, ofp)
Block *block;
FILE *ofp;
{
   int s;
   char ctemp[MAXNAME], *ptr;

   fprintf(ofp, "DR   BLOCKS;%s;%s\n", block->family, block->de);
   for (s=0; s< block->num_sequences; s++)
   {
         strcpy(ctemp, block->sequences[s].name);
         ptr = strtok(ctemp, "|");
         while (ptr != NULL)
	 {	
            fprintf(ofp, "%s\n", ptr);
            ptr = strtok(NULL, "|");
         }
   }

}  /* end of output_DR  */

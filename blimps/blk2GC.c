/* COPYRIGHT 2000 by the Fred Hutchinson Cancer Research Center
    blk2GC.c  	Makes list of sequences in blocks for GeneCard
           blk2GC <input blocks file>
	   Output to stdout
	Requires blimps libraries
--------------------------------------------------------------------
 4/ 4/00 J. Henikoff
====================================================================*/

#define EXTERN
#define MAXNAME 80	/* Maximum file name length */

#include <blocksprogs.h>

void output_GC();

/*=======================================================================*/

int main(argc, argv)
     int argc;
     char *argv[];
{
  FILE *bfp;
  Block *block;
  char bdbname[MAXNAME], prevfam[MAXNAME];

/* ------------1st arg = blocks database -------------------------------*/
   if (argc > 1)
      strcpy(bdbname, argv[1]);
   else
   {
      printf("\nEnter name of blocks database: ");
      gets(bdbname);
   }
   if ( (bfp=fopen(bdbname, "r")) == NULL)
   {
      printf("\nCannot open file %s\n", bdbname);
      exit(-1);
   }

/*-----------------------------------------------------------------*/
  prevfam[0] = '\0';
  while ((block = read_a_block(bfp)) != NULL)
  {
     if (strcmp(prevfam, block->family) != 0)  /* just process 1st block */
     {
        output_GC(block);
        strcpy(prevfam, block->family);
     }
  }
   
  fclose(bfp);

  exit(0);

}  /* end of main */
/*===================================================================
	Prints one line for each part of each sequence name
===================================================================*/
void output_GC(block)
Block *block;
{
   int s;
   char ctemp[MAXNAME], *ptr;

   for (s=0; s< block->num_sequences; s++)
   {
         strcpy(ctemp, block->sequences[s].name);
         ptr = strtok(ctemp, "|");
         while (ptr != NULL)
	 {	
            printf("%s | %s, %s;\n", ptr, block->family, block->de);
            ptr = strtok(NULL, "|");
         }
   }

}  /* end of output_DR  */

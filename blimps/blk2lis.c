/* COPYRIGHT 2000 by the Fred Hutchinson Cancer Research Center
    blk2lis.c  	Makes lists of sequences in blocks
           blk2lis <input blocks file>
	   Creates files named AC.lsb
--------------------------------------------------------------------
 3/28/00 J. Henikoff
====================================================================*/

#define BLNORM_C_
#define EXTERN
#define MAXNAME 80	/* Maximum file name length */

#include <blocksprogs.h>

void output_lis();

/*=======================================================================*/

int main(argc, argv)
     int argc;
     char *argv[];
{
  FILE *bfp;
  Block *block;
  char bdbname[MAXNAME], prevfam[MAXNAME];;

/* ------------1st arg = blocks database -------------------------------*/
   if (argc > 1)
      strcpy(bdbname, argv[1]);
   else
   {
      printf("\nEnter name of blocks database: ");
      gets(bdbname);
   }
   if ( (bfp=fopen(bdbname, "r")) == NULL)
   {
      printf("\nCannot open file %s\n", bdbname);
      exit(-1);
   }

/*-----------------------------------------------------------------*/

  prevfam[0] = '\0';
  while ((block = read_a_block(bfp)) != NULL)
  {
     if (strcmp(prevfam, block->family) != 0)  /* just process 1st block */
     {
        output_lis(block);
        strcpy(prevfam, block->family);
     }
  }
   
  fclose(bfp);
  exit(0);

}  /* end of main */
/*===================================================================
===================================================================*/
void output_lis(block)
Block *block;
{
   FILE *ofp;
   char outname[MAXNAME];
   int s;

   strcpy(outname, block->family);
   strcat(outname, ".lsb");
   if ( (ofp=fopen(outname, "w")) == NULL)
   {
         printf("\nCannot open file %s\n", outname);
         exit(-1);
   }
   else
   {
      fprintf(ofp, ">%s\n", block->family);
      fprintf(ofp, "pros/\n");
      for (s=0; s< block->num_sequences; s++)
         fprintf(ofp, "%s\tBLOCK\n", block->sequences[s].name);
      fclose(ofp);
   }

}  /* end of output_lis  */

/*    blk2slx.c Converts a block to Sean Eddy's slx format 
           blk2slx <input blocks file> <output slx file>
--------------------------------------------------------------------
 10/10/94 J. Henikoff
 12/23/06 Longer sequence name
====================================================================*/

#define BLK2SLX_C_
#define EXTERN
#define MAXNAME 80	/* Maximum file name length */

#include <blocksprogs.h>

/*
 * Local variables and data structures
 */
void output_slx();
/*=======================================================================*/
/*
 * main
 *   controls flow of program
 */

int main(argc, argv)
     int argc;
     char *argv[];
{
  FILE *bfp, *ofp;
  Block *block;
  char bdbname[MAXNAME], conname[MAXNAME];

/* ------------1st arg = blocks database -------------------------------*/
   if (argc > 1)
      strcpy(bdbname, argv[1]);
   else
   {
      printf("\nEnter name of blocks database: ");
      gets(bdbname);
   }
   if ( (bfp=fopen(bdbname, "r")) == NULL)
   {
      printf("\nCannot open file %s\n", bdbname);
      exit(-1);
   }
/* ------------2nd arg = slx blocks database ---------------------*/
   if (argc > 2)
      strcpy(conname, argv[2]);
   else
   {
      printf("\nEnter name of new slx database: ");
      gets(conname);
   }
   if ( (ofp=fopen(conname, "w")) == NULL)
   {
      printf("\nCannot open file %s\n", conname);
      exit(-1);
   }

/*-----------------------------------------------------------------*/

  while ((block = read_a_block(bfp)) != NULL)
  {
     output_slx(block, ofp);
  }
   
  fclose(bfp); fclose(ofp);
  exit(0);

}  /* end of main */
/*===================================================================
     slx format is sequence name, space, segment
====================================================================*/
void output_slx(block, ofp)
Block *block;
FILE *ofp;
{
   int seq, pos;
   for (seq=0; seq < block->num_sequences; seq++)
   {
      fprintf(ofp, "%-20s ", block->sequences[seq].name);
      for (pos=0; pos < block->width; pos++)
         fprintf(ofp, "%c", aa_btoa[ block->residues[seq][pos] ]);
      fprintf(ofp, "\n");
   }
}  /* end of output_slx */

/*  COPYRIGHT 1998, Fred Hutchinson Cancer Research Center
    fastaseqs.c  Read a file of sequences and converts them to FASTA format
           fastaseqs <input blocks file> <output file>
--------------------------------------------------------------------
 1/15/98 J. Henikoff
====================================================================*/

#define EXTERN
#define MAXNAME 80	/* Maximum file name length */

#include <blocksprogs.h>

/*=======================================================================*/
/*
 * main
 */

int main(argc, argv)
     int argc;
     char *argv[];
{
  FILE *fin, *fout;
  Sequence *seq;
  char infile[MAXNAME], outfile[MAXNAME];
  int db_type, seq_type, nseq;

   if (argc < 2)
   {
      printf("COPYRIGHT 1998, Fred Hutchinson Cancer Research Center\n");
      printf("fastaseqs: Converts a file of sequences to FASTA format\n");
      printf("USAGE: fastaseqs <input> <output>\n");
      printf("   <input>  = input file of seqences in a common format\n");
      printf("   <output> = output file of sequences in FASTA format\n");
   }

/* ------------1st arg = file of sequences------------------------------*/
   if (argc > 1)
      strcpy(infile, argv[1]);
   else
   {
      printf("\nEnter name of input sequence file: ");
      gets(infile);
   }
   if ( (fin=fopen(infile, "r")) == NULL)
   {
      printf("\nCannot open file %s\n", infile);
      exit(-1);
   }
/* ------------2nd arg = output file  -----------------------------------*/
   if (argc > 2)
      strcpy(outfile, argv[2]);
   else
   {
      printf("\nEnter name of output file: ");
      gets(outfile);
   }
   if ( (fout=fopen(outfile, "w")) == NULL)
   {
      printf("\nCannot open file %s\n", outfile);
      exit(-1);
   }

/*-----------------------------------------------------------------*/
   db_type = type_dbs(fin, DbInfo);
   if (db_type < 0) db_type = FLAT;
   seq_type = UNKNOWN_SEQ;
   seq_type = seq_type_dbs(fin, DbInfo, db_type, seq_type);

/*-----------------------------------------------------------------*/
/*   Have to read all the sequences into memory                    */

  rewind(fin);
  nseq = 0;
  while ( (seq = read_a_sequence(fin, db_type, seq_type)) != NULL)
  {
     nseq++;
     output_sequence(seq, fout);
     free_sequence(seq);
  }
   
  fclose(fin); fclose(fout);
  printf("%d sequences read\n", nseq);
  if (nseq > 0) exit(0);
  else          exit(-1);

}  /* end of main */

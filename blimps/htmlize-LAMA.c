/*  Copyright 1999-2003 Fred Hutchinson Cancer Research Center
	htmlize-LAMA.c

   This program will insert html links in LAMA output.
   Input - query blocks file name, target blocks file name, 
   LAMA output file name (default stdin), output file (default stdout) 

   Jan 97 - added link to "show_block_alignment", made use of icons. SP
   2/7/97 Check for btest in script name rather than using hard-coded names.JGH
   6/20/99 Renamed (used to be htmlink_LAMA-out) & updated for Blimps 3.2.6 &
	   for longer sequence names (10->18 chars).
*/

#include <stdlib.h>
#include <stdio.h>


#define TRUE 1
#define FALSE 0

#define MAXLINELEN 1000
#define SMALLBUFF   200

#define HELP_ICON "?"
#define LOGOS_ICON "<IMG SRC=\"/blocks/icons/logos.gif\" HEIGHT=\"13\" WIDTH=\"35\" ALT=\"Logos\">"
#define ALIGNMENT_ICON "<IMG SRC=\"/blocks/icons/aligned_blocks.gif\" HEIGHT=\"11\" WIDTH=\"21\" ALT=\"alignment\">"

  /* specific strings in score lines of lama output files. 
     Used for identifying score lines. */
#define SCORELINESTR1 " : "
#define SCORELINESTR2 ") score "

void dehtmlize_str() ;

/*
 * main
 */

int main(argc, argv)
     int argc;
     char *argv[];
{
  FILE   *inpf, *outf ;
  char   inpfname[MAXLINELEN], outfname[MAXLINELEN] ;
  char   readline[MAXLINELEN], prcssdline[MAXLINELEN] ;
  char   Qfname[MAXLINELEN], Tfname[MAXLINELEN] ;
  char   link_string1[SMALLBUFF], link_string2[SMALLBUFF], link_string3[SMALLBUFF] ;
  char   help_icon[SMALLBUFF], logos_icon[SMALLBUFF], alignment_icon[SMALLBUFF] ;
  char   block1[SMALLBUFF], block2[SMALLBUFF], *ptr, *script;
  int    pos_blk1, pos_blk2, algnmnt_len ;

   script = getenv("SCRIPT_NAME");
 
/* ------------ 1st arg = Query blocks file name ----------------------------*/
   if (argc > 1)
      strncpy(Qfname, argv[1], MAXLINELEN);
   else
      {
      fprintf(stderr, "%s query_blocks_file_name target_blocks_file_name LAMA_output_file_name\n",
             argv[0]) ;
      exit(-1) ;
      }

/* ------------ 2nd arg = Target blocks file name --------------------------*/
   if (argc > 2)
      strncpy(Tfname, argv[2], MAXLINELEN);
   else
      {
      fprintf(stderr, "%s query_blocks_file_name target_blocks_file_name LAMA_output_file_name\n",
             argv[0]) ;
      exit(-1) ;
      }

/* ------------ 3rd optional arg = output file ---------------------------*/
   if (argc > 3 && argv[3][0] != '-')
      {
      strncpy(inpfname, argv[3], MAXLINELEN);
      if ( (inpf=fopen(inpfname, "r")) == NULL)
         {
         fprintf(stderr, "Program %s cannot open input file %s\n", 
                argv[0], inpfname);
         exit(-3);
         }
      }
   else inpf = stdin ;

/*
   if (argc > 3)
      strncpy(inpfname, argv[3], MAXLINELEN);
   else
      {
      fprintf(stderr, "%s query_blocks_file_name target_blocks_file_name LAMA_output_file_name\n",
             argv[0]) ;
      exit(-1) ;
      }

   if ( (inpf=fopen(inpfname, "r")) == NULL)
      {
      fprintf(stderr, "Program %s cannot open input file %s\n", 
              argv[0], inpfname);
      exit(-2);
      }
*/

/* ------------ 4th optional arg = output file ---------------------------*/
   if (argc > 4 && argv[4][0] != '-')
      {
      strncpy(outfname, argv[4], MAXLINELEN);
      if ( (outf=fopen(outfname, "w")) == NULL)
         {
         fprintf(stderr, "Program %s cannot open output file %s\n", 
                argv[0], outfname);
         exit(-3);
         }
      }
   else outf = stdout ;

   if (strstr(script, "btest")) {
       sprintf(link_string1, "<A HREF=\"/btest-bin/LAMA_alignment.sh?");
     }
   else {
       sprintf(link_string1, "<A HREF=\"/blocks-bin/LAMA_alignment.sh?");
     }
   if (strstr(script, "btest")) {
       sprintf(link_string2, "<A HREF=\"/btest-bin/LAMA_logos.csh?");
     }
   else {
       sprintf(link_string2, "<A HREF=\"/blocks-bin/LAMA_logos.csh?");
     }
   if (strstr(script, "btest")) {
       sprintf(link_string3, "<A HREF=\"/btest/help/about_logos.html\">");
     }
   else  {
       sprintf(link_string3, "<A HREF=\"/blocks/help/about_logos.html\">");
     }

   strcpy(help_icon, HELP_ICON) ;
   strcpy(logos_icon, LOGOS_ICON) ;
   strcpy(alignment_icon, ALIGNMENT_ICON) ;

/* loop through all the input file lines */

   while ((fgets(readline, MAXLINELEN, inpf)) != NULL)
      {
                                           /* copy line without html code */
      dehtmlize_str(readline,prcssdline) ;

                                      /* identify if line is a score line */
      if (strstr(prcssdline,SCORELINESTR1) != NULL &&
          strstr(prcssdline,SCORELINESTR2) != NULL)
	 {
         sscanf(prcssdline, "%s %d %*c %*d %*s %s %d %*c %*d %*c%d ", 
               block1, &pos_blk1, block2, &pos_blk2, &algnmnt_len) ;

                                                        /* get rid of EOL */
         if ((ptr=strchr(readline,'\n')) != NULL) *ptr = '\0' ;
 
         fprintf(outf, "%s [%s%s+%s+%d+%s+%s+%d+%d\">%s</A> \
%s%s+%s+%d+%s+%s+%d+%d\">%s</A>%s%s</A>]\n", 
                readline, 
                link_string1,
                Qfname, block1, pos_blk1, 
                Tfname, block2, pos_blk2, algnmnt_len,
                alignment_icon,
                link_string2,
                Qfname, block1, pos_blk1, 
                Tfname, block2, pos_blk2, algnmnt_len,
                logos_icon,
                link_string3,
                help_icon) ;
         }
      else
         fprintf(outf,"%s", readline) ;
      }

   fclose(inpf) ;
   fclose(outf) ;

   exit(0);

}  /* end of main */



/* remove html code (anything bounded by '<' and '>' and the '<' + '>' 
   themselves */

void dehtmlize_str(instr,outstr) 

char instr[], outstr[] ;

{
   int i1, i2, htmlcode ;

   htmlcode = FALSE ;

   for(i1=0, i2=0; instr[i1] != '\0'; i1++)
      if (htmlcode)
	 {
         if (instr[i1] == '>') htmlcode = FALSE ;
         }
      else
	 {
         if (instr[i1] != '<') outstr[i2++] = instr[i1] ;
         else htmlcode = TRUE ;
         }

   outstr[i2] = '\0' ;

   return ;
}

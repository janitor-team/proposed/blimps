/*    Copyright 2007 Fred Hutchinson Cancer Research Center

	protxblk <input blocks file> <output file>

	Converts blocks to one line per protein with start/end
--------------------------------------------------------------------
  1/ 3/07 J. Henikoff
====================================================================*/

#define PROTXBLK_C_
#define EXTERN
#define MAXNAME 80	/* Maximum file name length */

#include <blocksprogs.h>

/*
 * Local variables and data structures
 */
void output_prot();
/*=======================================================================*/
/*
 * main
 *   controls flow of program
 */

int main(argc, argv)
     int argc;
     char *argv[];
{
  FILE *bfp, *ofp;
  Block *block;
  char bdbname[MAXNAME], conname[MAXNAME];

/* ------------1st arg = blocks database -------------------------------*/
   if (argc > 1)
      strcpy(bdbname, argv[1]);
   else
   {
      printf("\nEnter name of blocks database: ");
      gets(bdbname);
   }
   if ( (bfp=fopen(bdbname, "r")) == NULL)
   {
      printf("\nCannot open file %s\n", bdbname);
      exit(-1);
   }
/* ------------2nd arg = output file ---------------------*/
   if (argc > 2)
      strcpy(conname, argv[2]);
   else
   {
      printf("\nEnter name of output file: ");
      gets(conname);
   }
   if ( (ofp=fopen(conname, "w")) == NULL)
   {
      printf("\nCannot open file %s\n", conname);
      exit(-1);
   }

/*-----------------------------------------------------------------*/

  while ((block = read_a_block(bfp)) != NULL)
  {
     output_prot(block, ofp);
  }
   
  fclose(bfp); fclose(ofp);
  exit(0);

}  /* end of main */
/*===================================================================
     format is sequence name, block name, start, end, weight
====================================================================*/
void output_prot(block, ofp)
Block *block;
FILE *ofp;
{
   int seq, start, end;
   for (seq=0; seq < block->num_sequences; seq++)
   {
      start = block->sequences[seq].position;
      end = start + block->width - 1;
      fprintf(ofp, "%-20s\t%s\t%d\t%d\t%f\n", 
	block->sequences[seq].name, block->number, start, end,
	block->sequences[seq].weight);
   }
}  /* end of output_prot */

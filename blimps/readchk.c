/*    Read a PSI-BLAST checkpoint file into a blimps matrix structure */
#define EXTERN
#include <blocksprogs.h>

int main(argc, argv)
int argc;
char *argv[];
{
   FILE *chk;
   char chkname[80] ;
   Matrix *pssm;
   Sequence *seq;
   int pos, aa;
   long length;
   double dtemp;

   /*------------1st arg = PSI-BLAST checkpoint file--------------------*/
   if (argc > 1)
      strcpy(chkname, argv[1]);
   else
   {
      printf("\nEnter name of configuration file: ");
      gets(chkname);
   }
   if ( (chk=fopen(chkname, "r")) == NULL)
   {
      printf("\nCannot open file %s\n", chkname);
      exit(-1);
   }

   /* this is the length of the query seq = width of the pssm */
   fread(&length, sizeof(long), 1, chk);
   if (length > 0)
   { 
       pssm = new_matrix( (int) length);
       strcpy(pssm->id, "PSI-BLAST");
       strcpy(pssm->ac, "PSI-BLAST");
       strcpy(pssm->de, chkname);
       seq = (Sequence *) malloc(sizeof(Sequence));
       seq->max_length = seq->length = length;
       strcpy(seq->name, "PSI-BLAST");
       strcpy(seq->info, chkname);
       seq->position = 0;
       seq->weight = 100;
       seq->sequence = (Residue *) calloc(length, sizeof(Residue));
   }
   else
   { 
       printf("\nProblem reading %s\n", chkname);
       exit(-1);
   }
   /*  this is the query sequence */
   fread(seq->sequence, sizeof(char), length, chk);
   seq->sequence[length] = '\0';

   /* these are the target frequencies - are they sequence-weighted?
	have pseudo counts been added?   */
   for (pos=0; pos < length; pos++)
   {
      for (aa=1; aa < 21; aa++)
      {
         fread(&dtemp, sizeof(double), 1, chk);
         pssm->weights[aa][pos] = dtemp;
      }
   }

   output_matrix_st(pssm, stdout, FLOAT_OUTPUT);

   exit(0);
}  /* end of main */

/*  Copyright 2000 Fred Hutchinson Cancer Research Center
    Read a MAST log-odds file into a blimps matrix structure
	readmast <mast log-odds file> <I|F>

	Mast log-odds file is assumed to look like this:
ALPHABET= ARND...
log-odds matrix: alength= <na> w= <nw>
nw*na numbers
----------------------------------------------------------------------*/
#define EXTERN
#include <blocksprogs.h>

int main(argc, argv)
int argc;
char *argv[];
{
   FILE *chk;
   char chkname[80], line[MAXLINE], *ptr, *ptr1;
   Matrix *pssm;
   int pos, aa, alpha[MATRIX_AA_WIDTH], width, alength, itemp, okay, type;
   double dtemp, dmin;

   /*------------1st arg = MAST log-odds file--------------------*/
   if (argc > 1)
      strcpy(chkname, argv[1]);
   else
   {
      printf("\nEnter name of configuration file: ");
      gets(chkname);
   }
   if ( (chk=fopen(chkname, "r")) == NULL)
   {
      printf("\nCannot open file %s\n", chkname);
      exit(-1);
   }

   type = 1;	/* 0=> as-is, 1=>integer */
   for (aa=0; aa < MATRIX_AA_WIDTH; aa++) alpha[aa] = -1;
   alength = width = pos = 0;
   dmin = 9999999.99;
   pssm = NULL;
   okay = NO;

   while ( !feof(chk) && fgets(line, MAXLINE, chk) != NULL )
   {
      if (pssm != NULL) okay = YES;	/* have alphabet & width */

      if (strncmp(line, "ALPHABET", 7) == 0)
      {
	ptr = strtok(line, " ");
        if (ptr != NULL)
        {
		ptr = strtok(NULL, " \r\t\n");
		if (ptr != NULL) alength = (long) strlen(ptr);
		else alength = 0;
        }
        if (alength == 0)
        {
		printf("\nProblem finding ALPHABET in %s\n", chkname);
		exit(-1);
        }
	for (itemp=0; itemp<alength; itemp++)
	{
		aa = aa_atob[ ptr[itemp] ];
		if (aa >= 0 && aa < MATRIX_AA_WIDTH) alpha[itemp] = aa;
	}
      }   /* end of ALPHABET */

      /* alength= %d w= %d  */
      if (strncmp(line, "log-odds", 8) == 0)
      {
	ptr1 = strstr(line, "alength=");
        if (ptr1 != NULL)
        {
		sscanf(ptr1, "alength= %d w=%d", &itemp, &width);
		if (itemp != alength)
		{
		   printf("\nALPHABET length (%d) not alength (%d)\n", 
				alength, itemp);
		   exit(-1);
		}
        }
        if (width == 0)
        {
		printf("\nProblem finding width in %s\n", chkname);
		exit(-1);
        }
	else
	{
		pssm = new_matrix( (int) width);
		pssm->width = width;
		strcpy(pssm->id, "MAST");
		strcpy(pssm->ac, "MAST");
		strcpy(pssm->de, chkname);
		sprintf(pssm->ma, "width=%d;", width);
	}
      }  /* end of log-odds */

      if (okay)
      { 
	 ptr = strtok(line, " \t\r\n");
         for (aa=0; aa < alength; aa++)
         {
		if (ptr != NULL)
		{
			dtemp = atof(ptr);
			pssm->weights[ alpha[aa] ][pos] = dtemp;
			if (dtemp < dmin) dmin = dtemp;
			ptr = strtok(NULL, " \t\r\n");
		}
		else
		{
			printf("\nProblem finding value for pos=%d aa=%c\n",
				pos, aa_btoa[aa]);
		}
         }
	 pos++;
      }
   }   /* end of while */

   if (type)	/* positive integers */
   {
      /*  Normalize weights to add to 100 in each position */
      for (pos=0; pos<width; pos++)
      {
         pssm->weights[ aa_atob['B'] ][pos] = dmin;
         pssm->weights[ aa_atob['Z'] ][pos] = dmin;
         pssm->weights[ aa_atob['X'] ][pos] = dmin;
         pssm->weights[ aa_atob['*'] ][pos] = dmin;
         pssm->weights[ aa_atob['-'] ][pos] = dmin;
         if (dmin < 0)   /* make all weights positive */
         {
            dtemp = 0.0 - dmin;
            for (aa=0; aa<MATRIX_AA_WIDTH; aa++)
            { pssm->weights[aa][pos] = pssm->weights[aa][pos] + dtemp; }
         }
	 dtemp = 0.0;
         for (aa=0; aa<MATRIX_AA_WIDTH; aa++) 
         { dtemp += pssm->weights[aa][pos]; }
         for (aa=0; aa<MATRIX_AA_WIDTH; aa++) 
	 { pssm->weights[aa][pos] = 100.0 * pssm->weights[aa][pos] / dtemp; }
      }
      output_matrix_st(pssm, stdout, FLOAT_OUTPUT);
   }
   else  /* output it as-is */
   {
      output_matrix_st(pssm, stdout, FLOAT_OUTPUT);
   }

   exit(0);
}  /* end of main */

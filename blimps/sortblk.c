#define SORTBLK_C_
#define EXTERN
#define YES 1
#define NO 0
#define MAXNAME 80      /* Maximum file name length */
#define MAXAA 26        /* Dimension of aa_ arrays */
#define MAXINT 32000
#define ROUND(x) ((x >= 0.0) ? (int) (x+0.5) : (int) (x-0.5))
#define ORD(x) ((int) x - 65)
#define INDEX(n, col, row) (col*n - (col*(col+3))/2 - 1 + row)

#include <blocksprogs.h>
#include <sys/time.h>

/*
 * Local variables and data structures
 */

Block *sort_it();
void sort_in();
int namecmp();

/*=======================================================================*/
/*
 * main
 */

int main(argc, argv)
     int argc;
     char *argv[];
{
  FILE *bfp, *ofp;
  Block *block, *sblock;
  int wtype, clus, stype;
  char bdbname[MAXNAME], conname[MAXNAME], ctemp[10];
  struct timeval tv;

  ErrorLevelReport = 5;                 /* don't want to see them */
  /* initialize random number generator */
  gettimeofday(&tv, NULL);
  srandom(tv.tv_sec^tv.tv_usec);

  if (argc < 2)
  {
     printf("BLWEIGHT: Copyright 1994-2000 by the Fred Hutchinson");
     printf(" Cancer Research Center, \n\tSeattle, WA, USA\n");
     printf("Add sequence weights to blocks.\n");
     printf("USAGE: blweight <in-blocks> <out-blocks> <type> <scale>\n");
     printf("\t<in-blocks>=input file of blocks to weight\n");
     printf("\t<out-blocks>=output file of weighted blocks\n");
     printf("\t<type>=P (position-based) | V (Voronoi) | \n\t\t");
     printf("A (Vingron & Argos) | Cn (n percent clumping)\n");
     printf("\t<scale>=M (max=100) | N (sum=#seqs) | # (sum=#)\n");
  }
/* ------------1st arg = blocks database -------------------------------*/
   if (argc > 1)
      strcpy(bdbname, argv[1]);
   else
   {
      printf("\nEnter name of blocks database: ");
      gets(bdbname);
   }
   if ( (bfp=fopen(bdbname, "r")) == NULL)
   {
      printf("\nCannot open file %s\n", bdbname);
      exit(-1);
   }
/* ------------2nd arg = weighted blocks database ---------------------*/
   if (argc > 2)
      strcpy(conname, argv[2]);
   else
   {
      printf("\nEnter name of new sorted blocks database: ");
      gets(conname);
   }
   if ( (ofp=fopen(conname, "w")) == NULL)
   {
      printf("\nCannot open file %s\n", conname);
      exit(-1);
   }

   while ((block = read_a_block(bfp)) != NULL)
   {
/*
      sblock = sort_it(block);
      output_block(sblock,ofp);
*/
      sort_in(block);
      output_block(block,ofp);
   }
 
   fclose(bfp); fclose(ofp);
   exit(0);

}  /* end of main */

/*========================================================================*/
Block *sort_it(block)
Block *block;
{
   int iclus, npair, threshold, s1, s2, l1, l2, px, i, i1, i2, first;
   int width, nseq;
   Block *newblock;

   width = block->sequences[0].length;
   nseq = block->num_sequences;

   /*----------- Now rewrite the block ----------------------*/
   newblock = new_block(width, nseq);	/* allocates 1 Cluster */
   strcpy(newblock->id, block->id);
   strcpy(newblock->ac, block->ac);
   strcpy(newblock->number, block->number);
   strcpy(newblock->de, block->de);
   strcpy(newblock->bl, block->bl);
/*
   newblock->num_clusters = 0;
   newblock->max_clusters = iclus;
   newblock->clusters = (Cluster *) realloc(newblock->clusters, iclus*sizeof(Cluster));
*/

   for (s1=0; s1 < nseq; s1++)
   {
   }

   return(newblock);

}  /* end of sort_it */
/*========================================================================*/
void sort_in(block)
Block *block;
{
   qsort(block->sequences, block->num_sequences, sizeof(Sequence), namecmp);

   block->num_clusters = block->max_clusters = 1;
   block->clusters[0].num_sequences = block->num_sequences;

}  /* end of sort_in */
/*=======================================================================*/
int namecmp(s1, s2)
Sequence *s1, *s2;
{
   return(strcmp(s1->name, s2->name));
}

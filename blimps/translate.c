/*    translate.c   Read a sequence, translate it, write it out
--------------------------------------------------------------------
11/30/94
====================================================================*/

#define EXTERN
#define MAXNAME 80	/* Maximum file name length */

#include "blocksprogs.h"

/*=======================================================================*/
/*
 * main
 */

int main(argc, argv)
     int argc;
     char *argv[];
{
  FILE *bfp, *ofp;
  Sequence *sequence, *trans;
  char sname[MAXNAME], outname[MAXNAME], ctemp[MAXNAME];
  unsigned char gcode[64], revgcode[64];
  int db_type, seq_type, frame, code;

/* ------------1st arg = sequence database -------------------------------*/
   if (argc > 1)
      strcpy(sname, argv[1]);
   else
   {
      printf("\nEnter name of sequence database: ");
      gets(sname);
   }
   if ( (bfp=fopen(sname, "r")) == NULL)
   {
      printf("\nCannot open file %s\n", sname);
      exit(-1);
   }
   db_type = type_dbs(bfp, DbInfo);
   seq_type = seq_type_dbs(bfp, DbInfo, db_type);;
   if (seq_type != NA_SEQ)
   {
      printf("\nNot a nucleotide sequence\n");
      exit(-1);
   }

/* ------------2nd arg = translated database ---------------------*/
   if (argc > 2)
      strcpy(outname, argv[2]);
   else
   {
      printf("\nEnter name of new translated sequence database: ");
      gets(outname);
   }
   if ( (ofp=fopen(outname, "w")) == NULL)
   {
      printf("\nCannot open file %s\n", outname);
      exit(-1);
   }
/*----------------3rd arg = translation frame ---------------------*/
   if (argc > 3) frame = atoi(argv[3]);
   else
   {
      printf("\nEnter translation frame: ");
      gets(ctemp);
      frame = atoi(ctemp);
   }
   if (frame == 0 || frame < -3 || frame > 3) frame = 1;

/*----------------4rd arg = genetic code ---------------------*/
   if (argc > 4) code = atoi(argv[4]);
   else
   {
      for (code = 0; code < NUMBER_OF_GENETIC_CODES; code++)
          printf("\n %d %s", code, gcodes[code].name);
      printf("\nEnter genetic code: ");
      gets(ctemp);
      code = atoi(ctemp);
   }
   if (code < 0 || code > 8) code = 0;
   init_gcode(&gcodes[code], gcode, revgcode);

/*-----------------------------------------------------------------*/

  while ((sequence = read_a_sequence(bfp, db_type, seq_type)) != NULL)
  {
     trans = translate_sequence(sequence, frame, gcode, revgcode);
     output_sequence(trans, ofp);
  }

/*-----------------------------------------------------------------*/

  while ((sequence = read_a_sequence(bfp)) != NULL)
  {
     trans = translate_sequence(sequence, frame, gcode, revgcode);
     output_sequence(trans, ofp);
  }
   
  fclose(bfp); fclose(ofp);
  exit(0);

}  /* end of main */

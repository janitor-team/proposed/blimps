Source: blimps
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Laszlo Kajan <lkajan@rostlab.org>,
           Andreas Tille <tille@debian.org>
Section: non-free/science
XS-Autobuild: yes
Priority: optional
Build-Depends: debhelper (>= 12~),
               d-shlibs (>= 0.84~)
Standards-Version: 4.3.0
Vcs-Browser: https://salsa.debian.org/med-team/blimps
Vcs-Git: https://salsa.debian.org/med-team/blimps.git
Homepage: https://web.archive.org/web/20170606235903/http://blocks.fhcrc.org/blocks/uploads/blimps/

Package: blimps-utils
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends},
         libblimps3
Description: blocks database improved searcher
 BLIMPS (BLocks IMProved Searcher) is a searching tool that scores
 a protein sequence against blocks or a block against sequences.
 .
 This package contains the binaries.

Package: blimps-examples
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends}
Recommends: tcsh | csh
Enhances: libblimps3-dev,
          blimps-utils
Description: blocks database improved searcher (example data)
 BLIMPS (BLocks IMProved Searcher) is a searching tool that scores
 a protein sequence against blocks or a block against sequences.
 .
 This package contains example data.

Package: libblimps3-dev
Architecture: any
Section: non-free/libdevel
Depends: ${misc:Depends},
         libblimps3 (= ${binary:Version})
Suggests: blimps-utils
Provides: libblimps-dev
Conflicts: libblimps-dev
Description: blocks database improved searcher library (development)
 BLIMPS (BLocks IMProved Searcher) is a searching tool that scores
 a protein sequence against blocks or a block against sequences.
 .
 This package provides the library development headers and the static library.

Package: libblimps3
Architecture: any
Section: non-free/libs
Depends: ${shlibs:Depends},
         ${misc:Depends}
Suggests: blimps-utils
Description: blocks database improved searcher library
 BLIMPS (BLocks IMProved Searcher) is a searching tool that scores
 a protein sequence against blocks or a block against sequences.
 .
 This package provides the shared library.

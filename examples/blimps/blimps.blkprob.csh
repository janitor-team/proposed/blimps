#!/bin/csh
#    This is blimps.csh :  example of how to execute blimps & blkprob
#     Prompts for input, creates .cs file, executes blimps
#	blkprob looks for these files in the current directory:
#		blkprob.stp (documentation)
#		blksort.stn (percentiles for normalized scores)
#		blksort.bias (list of blocks with compositional bias)
#		repeats.dat (list of groups known to have repeated motifs)
#
unalias rm
limit coredumpsize 1k

#	Set this to where blimps was installed
setenv BLIMPS_DIR ../..

echo "BLIMPS Search Generator"
echo "One search will be started for each query..."
#
#     Get the search type   SE --- not needed, but used to figure
#                                  out SQ or BL vs DB
#
set search = BLOCK
echo "Search Types: BLOCK  => sequence vs Blocks Database"
echo "              MATRIX => block vs sequence database"
echo -n "Enter search type (BLOCK or MATRIX) [$search]: "
set answer = ($<)
if ($#answer != 0) then
   set search = $answer
endif
#
#     Get the query sequences  SQ or BL
#
set noglob
set all = $cwd/*.pro
echo "Enter query sequence(s) or matrices [$all]: "
set answer = ($<)
unset noglob
if ($#answer != 0) then
   set pros = ($answer)
else
   set pros = ($all)
endif
#
#   Get the database     DB
#
set noglob
if ($search == BLOCK) then
   set db = sample.blocks
else
   set db = /swiss/swiss-prot.dat
   echo "Sample database names: /genbank/gb*.seq"
endif
echo "Enter database name [$db]: "
set answer = ($<)
unset noglob
if ($#answer != 0) then
   set db = ($answer)
endif
echo $db
#
#   Get the frequency file   FR
#
set freq = $BLIMPS_DIR/docs/default.amino.frq
echo -n "Enter frequency file name [$freq]: "
set answer = ($<)
if ($#answer != 0) then
   set freq = $answer
endif
#   
#   Get the output location  OU
#
set out = $cwd
echo -n "Enter output directory [$out]: "
set answer = ($<)
if ($#answer != 0) then
   set out = $answer
endif
#
#   Get the file extension
#
set ext = bli
echo -n "Enter the output file extension [$ext]: "
set answer = ($<)
if ($#answer != 0) then
    set ext = $answer
endif
#
#	Get the number of results
#
if ($search == BLOCK) then
     set NU = "   0"
else if ($search == MATRIX) then
     set NU = " 500"
else set NU = " 350"
endif
echo -n "Enter the number of results to save (0 for default) [$NU]: "
set answer = ($<)
if ($#answer != 0) then
   set NU = $answer
endif
#
#   See if they want to change the parameters:
#             ERror_level, STrands, REpeats, NUmber_to_report
#
set ER = "   2"
set ST = "   2"
set CO = "   3"
if ($search == BLOCK) then
     echo "Keeping REpeats is turned on."
     set RE = "   1"
else 
     echo "Keeping REpeats is turned off."
     set RE = "   0"
endif
echo -n "Do you want to change the search parameters (y/n)? [n]: "
set answer = ($<)
if ($answer == "y" || $answer == "Y") then
   echo -n "Enter error message level parameter (ER) [$ER]: "
   set answer = ($<)
   if ($#answer != 0) then
      set ER = $answer
   endif
   echo -n "Enter strands parameter (ST) [$ST]: "
   set answer = ($<)
   if ($#answer != 0) then
      set ST = $answer
   endif
   echo -n "Enter repeat parameter, 1=yes, 0=none (RE) [$RE]: "
   set answer = ($<)
   if ($#answer != 0) then
      set RE = $answer
   endif
   echo -n "Enter number of results (NU) [$NU]: "
   set answer = ($<)
   if ($#answer != 0) then
      set NU = $answer
   endif
   echo -n "Enter sequence weighting option (CO) [$CO]: "
   set answer = ($<)
   if ($#answer != 0) then
      set CO = $answer
   endif
endif
#
#   Now start the searches
#
foreach pro ($pros)
   echo $pro
   set tpro = $pro:t
   set rpro = $tpro:r
   echo "Creating $rpro.cs..."
   echo "ER   $ER"           >$rpro.cs
   echo "FR   $freq"          >>$rpro.cs
   if ($search == MATRIX) then
      echo "BL   $pro"        >>$rpro.cs
   else
      echo "SQ   $pro"        >>$rpro.cs
   endif
   foreach d ($db)
      echo "DB   $d"             >>$rpro.cs
   end
   echo "OU   $out/$rpro.$ext" >>$rpro.cs
#   echo "SE   $search"         >>$rpro.cs
   echo "ST   $ST"             >>$rpro.cs
   echo "RE   $RE"             >>$rpro.cs
   echo "NU   $NU"             >>$rpro.cs
   echo "CO   $CO"             >>$rpro.cs
   echo "//"                   >>$rpro.cs
   echo "Executing blimps ..."
   date
#	Execute blimps & then blkprob if it was a BLOCK search
   echo "Executing blimps ... output in $out/$rpro.$ext"
   $BLIMPS_DIR/bin/blimps $rpro.cs  >/dev/null 
#	Other common blkprob options: Cutoff evalue: "-E 10"
   if ($search == BLOCK) then
      echo "Executing blkprob ... output in $out/$rpro.blkprob"
      $BLIMPS_DIR/bin/blkprob $rpro.cs >$out/$rpro.blkprob
   endif
#  rm $rpro.cs
end
exit(0)

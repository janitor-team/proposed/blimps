ERror_level		2			-- only WARNINGs and higher
BLock  			exampleA.blk		-- the query block
DBase			/bird/swiss/swiss37.uni
OUtput_file		exampleA.bli		-- the output file
STrands_to_search	2			-- if DNA, search both strands
REpeats_allowed		NO 			-- save only best alignment
NUmber_to_report	0			-- have blimps decide
COnversion_method       3                       -- the conversion method
//
------------------------------------------------------------------------
BLIMPS (BLocks IMProved Searcher)
(C) Copyright 1993, Fred Hutchinson Cancer Research Center

exampleA.cs -- A sample configuration file to search a Block vs
		a sequence database

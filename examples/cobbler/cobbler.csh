#!/bin/csh

#	Test cobbler installation

setenv BLIMPS_DIR ../..

set cfs = (*.cf)
foreach cf ($cfs)
echo "Running $cf..."
	$BLIMPS_DIR/bin/cobbler $cf
echo ""
end

set files = (distribution/*.*)
foreach file ($files)
	set test = $file:t
echo "Checking $test..."
	diff $test $file
echo ""
end
exit


#!/bin/csh
#		coduse.csh
#   How to create codon usage files for codehop
#
#   First get the CUTG codon usage files:
#	mkdir GB138 (or whatever the GenBank release number is)
# 	ftp://ftp.kazusa.or.jp/pub/codon/current/	
#	mget gb*.spsum

set bin = ../../bin

$bin/coduse GB138/gbpln.spsum "Arabidopsis thaliana" arab.codon.use
$bin/coduse GB138/gbpln.spsum "Amanita muscaria" amanita.codon.use
$bin/coduse GB138/gbpln.spsum "Brassica oleracea" broc.codon.use
$bin/coduse GB138/gbpln.spsum "Brassica napus" napus.codon.use
$bin/coduse GB138/gbpln.spsum "Oryza sativa" rice.codon.use
$bin/coduse GB138/gbpln.spsum "Nicotiana tabacum" tobacco.codon.use
$bin/coduse GB138/gbpln.spsum "Zea mays" zea.codon.use
$bin/coduse GB138/gbpln.spsum "Hordeum vulgare" barley.codon.use
$bin/coduse GB138/gbpln.spsum "Laccaria bicolor" laccaria.codon.use
$bin/coduse GB138/gbpln.spsum "Lycopersicon esculentum" tomato.codon.use
$bin/coduse GB138/gbpln.spsum "Mesembryanthemum crystallinum" iceplant.codon.use
$bin/coduse GB138/gbpln.spsum "Neurospora crassa" neur.codon.use
$bin/coduse GB138/gbpln.spsum "Porphyra pulchra" porph.codon.use
$bin/coduse GB138/gbpln.spsum "Chlamydomonas reinhardtii" chlamy.codon.use
$bin/coduse GB138/gbpln.spsum "Schizosaccharomyces pombe" pombe.codon.use
$bin/coduse GB138/gbpln.spsum "Saccharomyces cerevisiae" yeast.codon.use
$bin/coduse GB138/gbpln.spsum "Physcomitrella patens" moss.codon.use
$bin/coduse GB138/gbpln.spsum "Prunus persica" peach.codon.use
$bin/coduse GB138/gbpln.spsum "Ustilago maydis" ustilago.codon.use
$bin/coduse GB138/gbpln.spsum "Malus domestica" apple.codon.use
$bin/coduse GB138/gbpln.spsum "Solanum tuberosum" potato.codon.use
$bin/coduse GB138/gbpln.spsum "Gonyaulax polyedra" gony.codon.use
$bin/coduse GB138/gbpln.spsum "Pisum sativum" pea.codon.use
$bin/coduse GB138/gbpln.spsum "Euglena gracilis" euglena.codon.use
$bin/coduse GB138/gbpln.spsum "Gossypium hirsutum" gossy.codon.use
$bin/coduse GB138/gbpln.spsum "Clarkia breweri" clarkiab.codon.use
$bin/coduse GB138/gbpln.spsum "Clarkia concinna" clarkiac.codon.use
$bin/coduse GB138/gbpln.spsum "Dunaliella salina" dunal.codon.use
$bin/coduse GB138/gbpln.spsum "Marchantia polymorpha" marchan.codon.use
$bin/coduse GB138/gbpln.spsum "Chara corallina" chara.codon.use
$bin/coduse GB138/gbpln.spsum "Mucor racemosus" mucor.codon.use
$bin/coduse GB138/gbpln.spsum "Ceratopteris richardii" fern.codon.use
$bin/coduse GB138/gbpln.spsum "Antirrhinum majus" snapdragon.codon.use
#	NOTE:  mucorc.codon.use is a custom table from a user


$bin/coduse GB138/gbbct.spsum "Acholeplasma laidlawii" achole.codon.use
$bin/coduse GB138/gbbct.spsum "Chlorobium vibrioforme" chlvib.codon.use
$bin/coduse GB138/gbbct.spsum "Chlorobium tepidum" chltep.codon.use
$bin/coduse GB138/gbbct.spsum "Chlorobium limicola" chllim.codon.use
$bin/coduse GB138/gbbct.spsum "Haemophilus influenzae" haemophilus.codon.use
$bin/coduse GB138/gbbct.spsum "Escherichia coli" ecoli.codon.use
$bin/coduse GB138/gbbct.spsum "Ralstonia eutropha" ralston.codon.use
$bin/coduse GB138/gbbct.spsum "Rhodobacter sphaeroides" rhodo.codon.use
$bin/coduse GB138/gbbct.spsum "Streptomyces coelicolor" strep.codon.use
$bin/coduse GB138/gbbct.spsum "Lactobacillus casei" lacto.codon.use
$bin/coduse GB138/gbbct.spsum "Staphylococcus aureus" staph.codon.use
$bin/coduse GB138/gbbct.spsum "Bartonella bacilliformis" bartb.codon.use
$bin/coduse GB138/gbbct.spsum "Bartonella henselae" barth.codon.use
$bin/coduse GB138/gbbct.spsum "Brucella abortus" brucella.codon.use
$bin/coduse GB138/gbbct.spsum "Ehrlichia chaffeensis" ehrl.codon.use
$bin/coduse GB138/gbbct.spsum "Bacillus subtilis" bacsub.codon.use
$bin/coduse GB138/gbbct.spsum "Bacillus thuringiensis" bact.codon.use
$bin/coduse GB138/gbbct.spsum "Agrobacterium tumefaciens" agro.codon.use
$bin/coduse GB138/gbbct.spsum "Chlamydia trachomatis" chlam.codon.use
$bin/coduse GB138/gbbct.spsum "Paracoccus denitrificans" paraco.codon.use
$bin/coduse GB138/gbbct.spsum "Pseudomonas syringae pv. glycinea" pseudo.codon.use
$bin/coduse GB138/gbbct.spsum "Synechococcus sp." synecho.codon.use
$bin/coduse GB138/gbbct.spsum "Leptospira interrogans" lepto.codon.use
$bin/coduse GB138/gbbct.spsum "Chlorobium vibrioforme" greenv.codon.use

$bin/coduse GB138/gbinv.spsum "Giardia intestinalis" giardia.codon.use
$bin/coduse GB138/gbinv.spsum "Musca domestica" musca.codon.use
$bin/coduse GB138/gbinv.spsum "Tetrahymena thermophila" tetra.codon.use
$bin/coduse GB138/gbinv.spsum "Tetrahymena pyriformis" tetrapyr.codon.use
$bin/coduse GB138/gbinv.spsum "Trypanosoma brucei" tryp.codon.use
$bin/coduse GB138/gbinv.spsum "Leishmania major" leishm.codon.use
$bin/coduse GB138/gbinv.spsum "Leishmania tarentolae" leish.codon.use
$bin/coduse GB138/gbinv.spsum "Plasmodium falciparum" plasmo.codon.use
$bin/coduse GB138/gbinv.spsum "Schistosoma mansoni" schis.codon.use
$bin/coduse GB138/gbinv.spsum "Bombyx mori" bombyx.codon.use
$bin/coduse GB138/gbinv.spsum "Caenorhabditis elegans" worm.codon.use
$bin/coduse GB138/gbinv.spsum "Drosophila melanogaster" fly.codon.use
$bin/coduse GB138/gbinv.spsum "Hydra vulgaris" hydra.codon.use
$bin/coduse GB138/gbinv.spsum "Brugia malayi" brugia.codon.use
$bin/coduse GB138/gbinv.spsum "Onchocerca volvulus" onch.codon.use
$bin/coduse GB138/gbinv.spsum "Ascaris suum" ascaris.codon.use
$bin/coduse GB138/gbinv.spsum "Trichinella spiralis" trich.codon.use
$bin/coduse GB138/gbinv.spsum "Haemonchus contortus" haem.codon.use
$bin/coduse GB138/gbinv.spsum "Ancylostoma caninum" ancyl.codon.use
$bin/coduse GB138/gbinv.spsum "Lymnaea stagnalis" lymn.codon.use
$bin/coduse GB138/gbinv.spsum "Placopecten magellanicus" placo.codon.use
$bin/coduse GB138/gbinv.spsum "Strongylocentrotus purpuratus" strongy.codon.use
$bin/coduse GB138/gbinv.spsum "Hirudo medicinalis" hirudo.codon.use
$bin/coduse GB138/gbinv.spsum "Myzus persicae" myzus.codon.use
$bin/coduse GB138/gbinv.spsum "Heliothis virescens" helio.codon.use
$bin/coduse GB138/gbinv.spsum "Helicoverpa armigera" helicov.codon.use
$bin/coduse GB138/gbinv.spsum "Lucilia cuprina" luci.codon.use
$bin/coduse GB138/gbinv.spsum "Entamoeba histolytica" entam.codon.use
$bin/coduse GB138/gbinv.spsum "Geodia cydonium" geodia.codon.use
$bin/coduse GB138/gbinv.spsum "Suberites domuncula" suberites.codon.use
$bin/coduse GB138/gbinv.spsum "Heterodera glycines" soycyst.codon.use

$bin/coduse GB138/gbvrt.spsum "Danio rerio" zebra.codon.use
$bin/coduse GB138/gbvrt.spsum "Myxine glutinosa" ahagfish.codon.use
$bin/coduse GB138/gbvrt.spsum "Eptatretus stouti" phagfish.codon.use
#src/coduse GB121/gbvrt.spsum "Fugu rubripes" fugu.codon.use
$bin/coduse GB138/gbvrt.spsum "Takifugu rubripes" fugu.codon.use
$bin/coduse GB138/gbvrt.spsum "Xenopus laevis" frog.codon.use
$bin/coduse GB138/gbvrt.spsum "Ambystoma mexicanum" amby.codon.use
$bin/coduse GB138/gbvrt.spsum "Gallus gallus" chicken.codon.use
$bin/coduse GB138/gbvrt.spsum "Anas platyrhynchos" anas.codon.use
$bin/coduse GB138/gbvrt.spsum "Cairina moschata" duck.codon.use
$bin/coduse GB138/gbvrt.spsum "Anser anser" goose.codon.use

$bin/coduse GB138/gbrod.spsum "Cavia porcellus" cavia.codon.use
$bin/coduse GB138/gbrod.spsum "Mus musculus" mouse.codon.use
$bin/coduse GB138/gbrod.spsum "Rattus norvegicus" rat.codon.use

$bin/coduse GB138/gbmam.spsum "Bos taurus" cow.codon.use
$bin/coduse GB138/gbmam.spsum "Oryctolagus cuniculus" rabbit.codon.use
$bin/coduse GB138/gbmam.spsum "Ovis aries" sheep.codon.use
$bin/coduse GB138/gbmam.spsum "Sus scrofa" pig.codon.use

$bin/coduse GB138/gbpri.spsum "Homo sapiens" human.codon.use

$bin/coduse GB138/gbvrl.spsum "Chilo iridescent virus" civ.codon.use
$bin/coduse GB138/gbvrl.spsum "Human herpesvirus 4" herpes4.codon.use

unalias mv
mv *.codon.use ../../docs

exit
